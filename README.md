Role Name
=========

Install 'zsh + oh-my-zsh + powerlevel10k + plugins:zsh-autosuggestions,zsh-syntax-highlighting'

Based on 'romkatv/powerlevel10k/config/p10k-rainbow.zsh'.
Wizard options: nerdfont-complete + powerline, small icons, rainbow, unicode,
24h time, angled separators, slanted heads, flat tails, 1 line, compact, few icons,
concise, instant_prompt=verbose.

To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.

Requirements
------------

Tested on Ubuntu 20.04.5 LTS (GNU/Linux 5.4.0-125-generic x86_64)

Role Variables
--------------


Dependencies
------------

- git
- powerline  
- fonts-powerline
- zsh
- autojump

repo:
 - https://github.com/romkatv/powerlevel10k.git
 - https://github.com/zsh-users/zsh-autosuggestions
 - https://github.com/zsh-users/zsh-syntax-highlighting.git



Example Playbook
----------------

```- name: ansible-role-install-zsh
  hosts: servers
  roles:
    - ansible-role-install-zsh
```

License
-------

BSD

Author Information
------------------

AlexZa 2022
